package com.testapp.marvelcomics.common.di.modules;


import com.testapp.marvelcomics.data.ComicsSearchRepository;
import com.testapp.marvelcomics.data.local.LocalDBStorage;
import com.testapp.marvelcomics.data.remote.MarvelRemoteApi;
import com.testapp.marvelcomics.data.remote.MarvelRemoteStorage;
import com.testapp.marvelcomics.domain.SearchRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.annotations.NonNull;
import retrofit2.Retrofit;

@Module
public class RepositoryModule {

    @Singleton
    @NonNull
    @Provides
    MarvelRemoteStorage provideMarvelRepository() {
        return new MarvelRemoteStorage();
    }

    @Singleton
    @NonNull
    @Provides
    LocalDBStorage provideLocalDBStorage() {
        return new LocalDBStorage();
    }


    @Provides
    @NonNull
    @Singleton
    MarvelRemoteApi provideMarvelRemoteApi(Retrofit retrofit) {
        return retrofit.create(MarvelRemoteApi.class);
    }

    @Provides
    @NonNull
    @Singleton
    SearchRepository provideSearchRepository() {
        return new ComicsSearchRepository();
    }

}
