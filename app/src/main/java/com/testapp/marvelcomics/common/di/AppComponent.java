package com.testapp.marvelcomics.common.di;


import com.testapp.marvelcomics.common.di.modules.CommonModule;
import com.testapp.marvelcomics.common.di.modules.DomainModule;
import com.testapp.marvelcomics.common.di.modules.PresentersModule;
import com.testapp.marvelcomics.common.di.modules.RepositoryModule;
import com.testapp.marvelcomics.common.di.modules.RetrofitModule;
import com.testapp.marvelcomics.data.ComicsSearchRepository;
import com.testapp.marvelcomics.data.local.LocalDBStorage;
import com.testapp.marvelcomics.data.remote.MarvelRemoteStorage;
import com.testapp.marvelcomics.domain.ComicsSearchInteractor;
import com.testapp.marvelcomics.presentation.models.ComicsSearchModel;
import com.testapp.marvelcomics.presentation.presenters.ComicsHistoryListPresenter;
import com.testapp.marvelcomics.presentation.presenters.ComicsSearchListPresenter;
import com.testapp.marvelcomics.presentation.ui.activities.SearchComicsActivity;
import com.testapp.marvelcomics.presentation.ui.fragments.SearchHistoryFragment;
import com.testapp.marvelcomics.presentation.ui.fragments.SearchResultsListFragment;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {CommonModule.class, DomainModule.class, PresentersModule.class, RetrofitModule.class, RepositoryModule.class})
public interface AppComponent {

    void inject(SearchComicsActivity activity);

    void inject(LocalDBStorage activity);

    void inject(ComicsSearchRepository comicsSearchRepository);

    void inject(SearchResultsListFragment searchListFragment);

    void inject(ComicsSearchListPresenter comicsSearchListPresenter);

    void inject(ComicsSearchModel comicsSearchModel);

    void inject(ComicsSearchInteractor comicsSearchInteractor);

    void inject(MarvelRemoteStorage marvelRemoteStorage);

    void inject(SearchHistoryFragment searchHistoryFragment);

    void inject(ComicsHistoryListPresenter comicsHistoryListPresenter);

}

