package com.testapp.marvelcomics.common.di.modules;

import com.testapp.marvelcomics.domain.ComicsSearchInteractor;
import com.testapp.marvelcomics.domain.SearchInteractor;
import com.testapp.marvelcomics.domain.SearchModel;
import com.testapp.marvelcomics.presentation.models.ComicsSearchModel;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.annotations.NonNull;

@Module
public class DomainModule {

    @NonNull
    @Singleton
    @Provides
    SearchModel provideSearchModel() {
        return new ComicsSearchModel();
    }

    @NonNull
    @Singleton
    @Provides
    SearchInteractor provideSearchInteractor() {
        return new ComicsSearchInteractor();
    }


}
