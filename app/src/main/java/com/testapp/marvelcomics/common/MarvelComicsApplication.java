package com.testapp.marvelcomics.common;

import android.app.Application;

import com.testapp.marvelcomics.common.di.AppComponent;
import com.testapp.marvelcomics.common.di.DaggerAppComponent;
import com.testapp.marvelcomics.common.di.modules.CommonModule;
import com.testapp.marvelcomics.common.di.modules.DomainModule;
import com.testapp.marvelcomics.common.di.modules.PresentersModule;
import com.testapp.marvelcomics.common.di.modules.RepositoryModule;
import com.testapp.marvelcomics.common.di.modules.RetrofitModule;


public class MarvelComicsApplication extends Application {

    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        sAppComponent = DaggerAppComponent.builder()
                .commonModule(new CommonModule(this))
                .repositoryModule(new RepositoryModule())
                .presentersModule(new PresentersModule())
                .domainModule(new DomainModule())
                .retrofitModule(new RetrofitModule())
                .build();

    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

}
