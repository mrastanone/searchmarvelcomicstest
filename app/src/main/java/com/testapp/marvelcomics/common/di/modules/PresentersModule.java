package com.testapp.marvelcomics.common.di.modules;

import com.testapp.marvelcomics.presentation.models.HistoryListPresenter;
import com.testapp.marvelcomics.presentation.models.SearchListPresenter;
import com.testapp.marvelcomics.presentation.presenters.ComicsHistoryListPresenter;
import com.testapp.marvelcomics.presentation.presenters.ComicsSearchListPresenter;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.annotations.NonNull;

@Module
public class PresentersModule {

    @Singleton
    @NonNull
    @Provides
    SearchListPresenter provideSearchListPresenter() {
        return new ComicsSearchListPresenter();
    }

    @Singleton
    @NonNull
    @Provides
    HistoryListPresenter provideHistoryListPresenter() {
        return new ComicsHistoryListPresenter();
    }
}
