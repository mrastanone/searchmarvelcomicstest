package com.testapp.marvelcomics.data.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface DaoAccess {

    @Query("SELECT * FROM SearchRequestParams ORDER BY saveTimeMilliseconds")
    Flowable<List<SearchRequestParams>> getSearchHistory();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void saveSearchRequest(SearchRequestParams searchRequestParams);
}
