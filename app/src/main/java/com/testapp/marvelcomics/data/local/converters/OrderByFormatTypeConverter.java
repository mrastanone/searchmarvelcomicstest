package com.testapp.marvelcomics.data.local.converters;

import android.arch.persistence.room.TypeConverter;

import com.testapp.marvelcomics.domain.dto.AnnotationOrderBy;

public class OrderByFormatTypeConverter {

    @TypeConverter
    public AnnotationOrderBy convertStringToOrderByType(String value) {
        return value.isEmpty() ? null : new AnnotationOrderBy(value);
    }

    @TypeConverter
    public String convertOrderByToStringType(AnnotationOrderBy orderBy) {
        return orderBy != null ? orderBy.getOrderBy() : "";
    }
}
