package com.testapp.marvelcomics.data.remote;

import com.testapp.marvelcomics.data.remote.pojo.MarvelComicsListResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.QueryMap;

public interface MarvelRemoteApi {

    // Added custom header parameters, because server can't identify request from application and require 'hash' in GET parameters,
    // like on server - server connection
    @Headers({"Accept: application/json",
            "Host: gateway.marvel.com",
            "Origin: https://developer.marvel.com",
            "Referer: https://developer.marvel.com/docs"})

    @GET("/v1/public/comics")
    Observable<MarvelComicsListResponse> getComicsBySearchName(@QueryMap Map<String, String> options);
}
