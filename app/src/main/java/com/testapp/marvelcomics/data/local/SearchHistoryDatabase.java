package com.testapp.marvelcomics.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;

import com.testapp.marvelcomics.data.local.converters.ComicFormatTypeConverter;
import com.testapp.marvelcomics.data.local.converters.OrderByFormatTypeConverter;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

@Database(entities = {SearchRequestParams.class}, version = 1, exportSchema = false)
@TypeConverters({ComicFormatTypeConverter.class, OrderByFormatTypeConverter.class})
abstract class SearchHistoryDatabase extends RoomDatabase {

    abstract DaoAccess daoAccess();

}
