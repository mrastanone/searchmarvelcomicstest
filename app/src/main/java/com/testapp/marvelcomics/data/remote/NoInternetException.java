package com.testapp.marvelcomics.data.remote;

import java.io.IOException;

public class NoInternetException extends IOException {
}
