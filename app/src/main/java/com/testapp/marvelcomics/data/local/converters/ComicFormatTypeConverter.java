package com.testapp.marvelcomics.data.local.converters;

import android.arch.persistence.room.TypeConverter;

import com.testapp.marvelcomics.domain.dto.AnnotationComicFormat;

public class ComicFormatTypeConverter {

    @TypeConverter
    public AnnotationComicFormat storedStringToComicFormat(String value) {
        return value.isEmpty() ? null : new AnnotationComicFormat(value);
    }

    @TypeConverter
    public String languagesToStoredString(AnnotationComicFormat comicFormat) {
        return comicFormat != null ? comicFormat.getFormat() : "";
    }
}
