package com.testapp.marvelcomics.data.remote;

import com.testapp.marvelcomics.common.MarvelComicsApplication;
import com.testapp.marvelcomics.data.remote.pojo.MarvelComicsListResponse;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.HashMap;

import javax.inject.Inject;

import io.reactivex.Observable;


public class MarvelRemoteStorage {

    private static final String API_PUBLIC_KEY = "6c23a0934773eec1d9960f41e3437ef0";
    private static final int ITEMS_LIMIT = 10;
    private static final String LIMIT = "limit";
    private static final String API_KEY = "apikey";
    private static final String OFFSET = "offset";

    public static final String TITLE = "title";
    public static final String TITLE_START_WITH = "titleStartsWith";
    public static final String FORMAT = "format";
    public static final String ORDER_BY = "orderBy";
    public static final String ISSUE_NUMBER = "issueNumber";
    public static final String NO_VARIANTS = "noVariants";

    @Inject
    MarvelRemoteApi marvelRemoteApi;

    private SearchRequestParams searchRequestParams;
    private int offset;

    public MarvelRemoteStorage() {
        MarvelComicsApplication.getAppComponent().inject(this);
    }

    public Observable<MarvelComicsListResponse> requestComicsBySearchKey(SearchRequestParams searchRequestParams) {
        this.searchRequestParams = searchRequestParams;
        this.offset = 0;

        HashMap<String, String> paramsMap = searchRequestParams.convertParamsToMap();
        paramsMap.put(LIMIT, String.valueOf(ITEMS_LIMIT));
        paramsMap.put(API_KEY, API_PUBLIC_KEY);

        return marvelRemoteApi.getComicsBySearchName(paramsMap);
    }

    public Observable<MarvelComicsListResponse> requestMoreComicsByPreviousSearchKey() {
        offset = offset + ITEMS_LIMIT;

        HashMap<String, String> paramsMap = searchRequestParams.convertParamsToMap();
        paramsMap.put(LIMIT, String.valueOf(ITEMS_LIMIT));
        paramsMap.put(OFFSET, String.valueOf(offset));
        paramsMap.put(API_KEY, API_PUBLIC_KEY);

        return marvelRemoteApi.getComicsBySearchName(paramsMap);
    }
}
