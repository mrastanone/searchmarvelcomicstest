package com.testapp.marvelcomics.data;

import com.testapp.marvelcomics.common.MarvelComicsApplication;
import com.testapp.marvelcomics.data.local.LocalDBStorage;
import com.testapp.marvelcomics.data.remote.MarvelRemoteStorage;
import com.testapp.marvelcomics.data.remote.pojo.MarvelComicsListResponse;
import com.testapp.marvelcomics.domain.SearchRepository;
import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ComicsSearchRepository implements SearchRepository {

    @Inject
    MarvelRemoteStorage marvelRemoteStorage;

    @Inject
    LocalDBStorage localDBStorage;

    public ComicsSearchRepository() {
        MarvelComicsApplication.getAppComponent().inject(this);
    }

    @Override
    public Observable<List<Comic>> requestComicsBySearchParams(SearchRequestParams searchRequestParams) {
        return marvelRemoteStorage.requestComicsBySearchKey(searchRequestParams)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(marvelComicsListResponse -> marvelComicsListResponse.data.results);
    }

    @Override
    public Observable<List<Comic>> requestMoreComicsByPreviousSearch() {
        return marvelRemoteStorage.requestMoreComicsByPreviousSearchKey()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .map(marvelComicsListResponse -> marvelComicsListResponse.data.results);
    }

    @Override
    public Flowable<List<SearchRequestParams>> getSearchHistory() {
        return localDBStorage.getSearchHistory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public void saveSearchRequest(final SearchRequestParams searchRequestParams) {
        new Thread(() -> localDBStorage.saveSearchRequest(searchRequestParams)).start();
    }
}
