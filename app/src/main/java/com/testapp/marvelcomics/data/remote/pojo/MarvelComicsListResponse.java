package com.testapp.marvelcomics.data.remote.pojo;

import com.google.gson.annotations.SerializedName;
import com.testapp.marvelcomics.domain.dto.Comic;

import java.util.List;

public class MarvelComicsListResponse {

    @SerializedName("data")
    public Data data;

    public class Data {

        @SerializedName("results")
        public List<Comic> results;
    }
}
