package com.testapp.marvelcomics.data.local;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.testapp.marvelcomics.common.MarvelComicsApplication;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;

public class LocalDBStorage {

    @Inject
    Context context;

    private static final String DATABASE_NAME = "LOCATION_DB";
    private SearchHistoryDatabase searchHistoryDatabase;

    public LocalDBStorage() {
        MarvelComicsApplication.getAppComponent().inject(this);
        searchHistoryDatabase = Room.databaseBuilder(context, SearchHistoryDatabase.class, DATABASE_NAME).build();
    }

    public Flowable<List<SearchRequestParams>> getSearchHistory(){
        return searchHistoryDatabase.daoAccess().getSearchHistory();
    }

    public void saveSearchRequest(SearchRequestParams searchRequestParams){
        searchRequestParams.saveTimeMilliseconds = System.currentTimeMillis();
        searchRequestParams.id = 0;
        searchHistoryDatabase.daoAccess().saveSearchRequest(searchRequestParams);
    }

}
