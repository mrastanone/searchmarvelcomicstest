package com.testapp.marvelcomics.presentation.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.testapp.marvelcomics.R;
import com.testapp.marvelcomics.domain.dto.Comic;

import java.util.ArrayList;
import java.util.List;

public class ComicsListAdapter extends RecyclerView.Adapter<ComicsListAdapter.ViewHolder> {

    private static final int ITEM_TYPE_REGULAR = 1;
    private static final int ITEM_TYPE_LOADING = 2;

    public interface ComicsListAdapterItemClickListener {

        void onItemClick(Comic comic);

    }

    private final Context context;
    private final RequestOptions glideOptions;
    private List<Comic> comics = new ArrayList<>();
    private ComicsListAdapterItemClickListener listener;
    private boolean isVisibleLoadingIndicator = true;

    @SuppressLint("CheckResult")
    public ComicsListAdapter(Context context) {
        this.context = context;
        glideOptions = new RequestOptions();
        glideOptions.centerCrop();
        glideOptions.placeholder(R.drawable.marvel_placeholder);
    }

    @NonNull
    @Override
    public ComicsListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemRootView = null;
        switch (viewType) {
            case ITEM_TYPE_REGULAR:
                itemRootView = LayoutInflater.from(context).inflate(R.layout.item_comics_list, parent, false);
                break;
            case ITEM_TYPE_LOADING:
                itemRootView = LayoutInflater.from(context).inflate(R.layout.item_comics_list_loading, parent, false);
                break;
        }
        return new ViewHolder(itemRootView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position < comics.size()) {
            final Comic item = comics.get(position);
            holder.title.setText(item.title);
            holder.description.setText(item.description);

            if (!item.thumbnail.path.contains("image_not_available")) {
                Glide.with(context)
                        .load(item.thumbnail.path + "." + item.thumbnail.extension)
                        .apply(glideOptions)
                        .into(holder.cover);
            } else {
                holder.cover.setImageDrawable(context.getResources().getDrawable(R.drawable.marvel_placeholder));
            }

            holder.cardView.setOnClickListener(view -> {
                if (listener != null)
                    listener.onItemClick(item);
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        return position < comics.size() ? ITEM_TYPE_REGULAR : ITEM_TYPE_LOADING;
    }

    @Override
    public int getItemCount() {
        return isVisibleLoadingIndicator ? comics.size() + 1 : comics.size();
    }

    public int getComicsListCount() {
        return comics.size();
    }

    public void setListener(ComicsListAdapterItemClickListener listener) {
        this.listener = listener;
    }

    public void addComics(List<Comic> comics) {
        this.comics.addAll(comics);
        notifyDataSetChanged();
    }

    public void clearAdapter() {
        this.comics.clear();
        notifyDataSetChanged();
    }

    public void setLoadingIndicatorVisibility(boolean isVisibleLoadingIndicator) {
        this.isVisibleLoadingIndicator = isVisibleLoadingIndicator;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView title;
        TextView description;
        ImageView cover;
        CardView cardView;

        ViewHolder(View viewGroup) {
            super(viewGroup);
            title = viewGroup.findViewById(R.id.comicTitle);
            description = viewGroup.findViewById(R.id.comicDescription);
            cover = viewGroup.findViewById(R.id.comicCover);
            cardView = viewGroup.findViewById(R.id.cardView);
        }
    }
}
