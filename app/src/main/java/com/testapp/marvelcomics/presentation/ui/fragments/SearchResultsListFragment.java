package com.testapp.marvelcomics.presentation.ui.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.testapp.marvelcomics.R;
import com.testapp.marvelcomics.common.MarvelComicsApplication;
import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.models.SearchListPresenter;
import com.testapp.marvelcomics.presentation.ui.activities.SearchFiltersActivity;
import com.testapp.marvelcomics.presentation.ui.adapters.ComicsListAdapter;
import com.testapp.marvelcomics.presentation.views.SearchResultsListView;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

public class SearchResultsListFragment extends Fragment implements SearchResultsListView {

    private static final int CREATE_FILTER_PARAMS_REQUEST_CODE = 100;
    public static final String FILTER_PARAMS_PARAM_KEY = "FILTERS_PARAMS_PARAM_KEY";


    @Inject
    SearchListPresenter presenter;

    private View rootView;
    private ComicsListAdapter comicsAdapter;
    private SearchRequestParams searchRequestParams;
    private int totalItemCount;
    private int lastVisibleItem;
    private boolean isLoading;
    private TextView emptyListView;
    private RecyclerView recyclerView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        searchRequestParams = new SearchRequestParams();
        MarvelComicsApplication.getAppComponent().inject(this);
        presenter.bindLifecycleOwner(this);
        presenter.bindView(this);
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_comics_list, container, false);

        recyclerView = rootView.findViewById(R.id.comicsList);
        comicsAdapter = new ComicsListAdapter(getContext());
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(comicsAdapter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                if (!isLoading && totalItemCount == (lastVisibleItem + 1)) {
                    presenter.loadMoreComics();
                    isLoading = true;
                }
            }
        });

        emptyListView = rootView.findViewById(R.id.empty_list_hint);

        rootView.findViewById(R.id.btnFilters).setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), SearchFiltersActivity.class);
            intent.putExtra(FILTER_PARAMS_PARAM_KEY, searchRequestParams);
            startActivityForResult(intent, CREATE_FILTER_PARAMS_REQUEST_CODE);
        });

        isLoading = true;
        presenter.requestComicsBySearchParams(searchRequestParams, false);

        return rootView;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CREATE_FILTER_PARAMS_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Serializable requestParams = data.getExtras().getSerializable(FILTER_PARAMS_PARAM_KEY);
            if (requestParams != null) {
                searchRequestParams = (SearchRequestParams) requestParams;
                isLoading = true;
                comicsAdapter.setLoadingIndicatorVisibility(true);
                presenter.requestComicsBySearchParams(searchRequestParams, true);
            }
        }
    }

    @Override
    public void setActualSearchParameters(SearchRequestParams searchParameters) {
        if (searchParameters != null) {
            this.searchRequestParams = searchParameters;
        }
    }

    @Override
    public void addComics(List<Comic> comics) {
        isLoading = false;
        comicsAdapter.addComics(comics);

        if (comics.isEmpty() && comicsAdapter.getComicsListCount() == 0) {
            comicsAdapter.setLoadingIndicatorVisibility(false);
            emptyListView.setVisibility(View.VISIBLE);
        } else if (comics.isEmpty() && comicsAdapter.getItemCount() > 1) {
            comicsAdapter.setLoadingIndicatorVisibility(false);
        } else if (!comics.isEmpty()) {
            emptyListView.setVisibility(View.INVISIBLE);
            comicsAdapter.setLoadingIndicatorVisibility(true);
        }
    }

    @Override
    public void clearComicsList() {
        comicsAdapter.clearAdapter();
    }

    @Override
    public void showError() {
        comicsAdapter.setLoadingIndicatorVisibility(false);
        Snackbar.make(rootView, getString(R.string.error_message), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showNoInternetConnectionMessage() {
        comicsAdapter.setLoadingIndicatorVisibility(false);
        Snackbar.make(rootView, getString(R.string.no_internet), Snackbar.LENGTH_LONG).show();
    }
}