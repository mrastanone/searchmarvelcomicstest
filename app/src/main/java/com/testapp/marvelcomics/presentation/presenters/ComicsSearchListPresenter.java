package com.testapp.marvelcomics.presentation.presenters;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.testapp.marvelcomics.common.MarvelComicsApplication;
import com.testapp.marvelcomics.data.remote.NoInternetException;
import com.testapp.marvelcomics.domain.SearchModel;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.models.SearchListPresenter;
import com.testapp.marvelcomics.presentation.views.SearchResultsListView;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ComicsSearchListPresenter implements SearchListPresenter, LifecycleObserver {

    @Inject
    SearchModel searchModel;

    private Disposable subscription;
    private SearchResultsListView searchResultsListView;
    private SearchRequestParams lastSearchRequestParams;

    public ComicsSearchListPresenter() {
        MarvelComicsApplication.getAppComponent().inject(this);
        searchModel.bindSearchListPresenter(this);
    }

    public ComicsSearchListPresenter(SearchModel searchModel) {
        this.searchModel = searchModel;
    }

    public void bindLifecycleOwner(LifecycleOwner lifecycleOwner) {
        lifecycleOwner.getLifecycle().addObserver(this);
    }

    public void bindView(SearchResultsListView searchResultsListView) {
        this.searchResultsListView = searchResultsListView;
        if (lastSearchRequestParams != null) {
            searchResultsListView.setActualSearchParameters(lastSearchRequestParams);
        }
    }

    @Override
    public void requestComicsBySearchParams(SearchRequestParams searchRequestParams, boolean isSaveRequest) {
        lastSearchRequestParams = searchRequestParams;
        if (searchResultsListView != null) {
            searchResultsListView.clearComicsList();
            searchResultsListView.setActualSearchParameters(searchRequestParams);
        }
        subscription = searchModel.requestComicsBySearchParams(searchRequestParams, isSaveRequest)
                .subscribe(comics -> {
                    if (searchResultsListView != null) {
                        searchResultsListView.addComics(comics);
                    }
                }, throwable -> {
                    if (searchResultsListView != null) {
                        if (throwable instanceof NoInternetException) {
                            searchResultsListView.showNoInternetConnectionMessage();
                        } else {
                            searchResultsListView.showError();
                        }
                    }
                });
    }

    @Override
    public void loadMoreComics() {
        subscription = searchModel.requestMoreComicsByPreviousSearchRequest()
                .subscribe(comics -> {
                    if (searchResultsListView != null) {
                        searchResultsListView.addComics(comics);
                    }
                }, throwable -> {
                    if (searchResultsListView != null) {
                        if (throwable instanceof NoInternetException) {
                            searchResultsListView.showNoInternetConnectionMessage();
                        } else {
                            searchResultsListView.showError();
                        }
                    }
                });
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onViewDestroy() {
        if (subscription != null) {
            subscription.dispose();
        }
        searchResultsListView = null;
    }
}
