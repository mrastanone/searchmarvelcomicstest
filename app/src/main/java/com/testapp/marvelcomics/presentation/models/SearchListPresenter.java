package com.testapp.marvelcomics.presentation.models;

import android.arch.lifecycle.LifecycleOwner;

import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.views.SearchResultsListView;

public interface SearchListPresenter {

    void requestComicsBySearchParams(SearchRequestParams searchRequestParams, boolean isSaveRequest);

    void loadMoreComics();

    void bindLifecycleOwner(LifecycleOwner lifecycleOwner);

    void bindView(SearchResultsListView searchResultsListView);

}
