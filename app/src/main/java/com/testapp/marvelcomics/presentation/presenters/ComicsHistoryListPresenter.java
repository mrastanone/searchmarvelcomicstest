package com.testapp.marvelcomics.presentation.presenters;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.OnLifecycleEvent;

import com.testapp.marvelcomics.common.MarvelComicsApplication;
import com.testapp.marvelcomics.domain.SearchModel;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.models.HistoryListPresenter;
import com.testapp.marvelcomics.presentation.views.HistorySearchListView;

import javax.inject.Inject;

import io.reactivex.disposables.Disposable;

public class ComicsHistoryListPresenter implements HistoryListPresenter, LifecycleObserver {

    @Inject
    SearchModel searchModel;

    private Disposable subscription;
    private HistorySearchListView searchResultsListView;

    public ComicsHistoryListPresenter(SearchModel searchModel) {
        this.searchModel = searchModel;
    }

    public ComicsHistoryListPresenter() {
        MarvelComicsApplication.getAppComponent().inject(this);
    }

    @Override
    public void bindLifecycleOwner(LifecycleOwner lifecycleOwner) {
        lifecycleOwner.getLifecycle().addObserver(this);
    }

    @Override
    public void bindView(HistorySearchListView searchResultsListView) {
        this.searchResultsListView = searchResultsListView;
    }


    @Override
    public void loadSearchHistory() {
        subscription = searchModel.getSearchHistory()
                .subscribe(historyList -> {
                    if (searchResultsListView != null) {
                        searchResultsListView.clearHistoryList();
                        searchResultsListView.showHistoryList(historyList);
                    }
                }, throwable -> {
                    if (searchResultsListView != null) {
                        searchResultsListView.showError();
                    }
                });
    }

    @Override
    public void historySearchItemSelected(SearchRequestParams requestParams) {
        searchModel.historyItemSelected(requestParams);
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    public void onViewDestroy() {
        if (subscription != null) {
            subscription.dispose();
        }
        searchResultsListView = null;
    }
}
