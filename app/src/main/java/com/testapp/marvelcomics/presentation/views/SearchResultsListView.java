package com.testapp.marvelcomics.presentation.views;

import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.List;

public interface SearchResultsListView {

    void setActualSearchParameters(SearchRequestParams searchParameters);

    void addComics(List<Comic> comics);

    void clearComicsList();

    void showError();

    void showNoInternetConnectionMessage();
}
