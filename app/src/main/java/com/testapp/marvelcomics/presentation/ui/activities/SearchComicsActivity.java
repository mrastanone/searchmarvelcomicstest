package com.testapp.marvelcomics.presentation.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.testapp.marvelcomics.R;
import com.testapp.marvelcomics.presentation.ui.adapters.SearchComicsPagerAdapter;
import com.testapp.marvelcomics.presentation.ui.fragments.SearchHistoryFragment;
import com.testapp.marvelcomics.presentation.ui.fragments.SearchResultsListFragment;

public class SearchComicsActivity extends AppCompatActivity {

    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_comics);

        viewPager = findViewById(R.id.searchViewPager);
        SearchComicsPagerAdapter adapter = new SearchComicsPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new SearchResultsListFragment(), getString(R.string.comics));
        adapter.addFrag(new SearchHistoryFragment(), getString(R.string.history));
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.searchTabs);
        tabLayout.setupWithViewPager(viewPager);
    }

    public void setCurrentTab(int tabPosition) {
        viewPager.setCurrentItem(tabPosition);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
