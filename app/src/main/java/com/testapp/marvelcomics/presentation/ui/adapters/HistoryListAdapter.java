package com.testapp.marvelcomics.presentation.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.testapp.marvelcomics.R;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class HistoryListAdapter extends RecyclerView.Adapter<HistoryListAdapter.ViewHolder> {


    public interface HistoryListAdapterItemClickListener {

        void onItemClick(SearchRequestParams searchRequestParams);

    }

    private final Context context;
    private List<SearchRequestParams> requests = new ArrayList<>();
    private HistoryListAdapter.HistoryListAdapterItemClickListener listener;
    private final SimpleDateFormat dateFormat;

    @SuppressLint("CheckResult")
    public HistoryListAdapter(Context context) {
        this.context = context;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss", Locale.getDefault());
    }

    @NonNull
    @Override
    public HistoryListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemRootView = LayoutInflater.from(context).inflate(R.layout.item_history_list, parent, false);
        return new HistoryListAdapter.ViewHolder(itemRootView);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryListAdapter.ViewHolder holder, int position) {
        final SearchRequestParams item = requests.get(position);
        holder.date.setText(dateFormat.format(new Date(item.saveTimeMilliseconds)));

        holder.cardView.setOnClickListener(view -> {
            if (listener != null)
                listener.onItemClick(item);
        });
    }


    @Override
    public int getItemCount() {
        return requests.size();
    }

    public void setListener(HistoryListAdapterItemClickListener listener) {
        this.listener = listener;
    }

    public void addItems(List<SearchRequestParams> searchRequestParamsList) {
        this.requests.addAll(searchRequestParamsList);
        notifyDataSetChanged();
    }

    public void clearAdapter() {
        this.requests.clear();
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView date;
        CardView cardView;

        ViewHolder(View viewGroup) {
            super(viewGroup);
            date = viewGroup.findViewById(R.id.textViewSearchDate);
            cardView = viewGroup.findViewById(R.id.cardView);
        }
    }
}
