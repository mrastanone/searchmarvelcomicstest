package com.testapp.marvelcomics.presentation.models;

import android.arch.lifecycle.LifecycleOwner;

import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.views.HistorySearchListView;

public interface HistoryListPresenter {

    void historySearchItemSelected(SearchRequestParams requestParams);

    void loadSearchHistory();

    void bindLifecycleOwner(LifecycleOwner lifecycleOwner);

    void bindView(HistorySearchListView searchResultsListView);
}
