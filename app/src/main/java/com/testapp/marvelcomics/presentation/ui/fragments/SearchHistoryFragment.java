package com.testapp.marvelcomics.presentation.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.testapp.marvelcomics.R;
import com.testapp.marvelcomics.common.MarvelComicsApplication;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.models.HistoryListPresenter;
import com.testapp.marvelcomics.presentation.ui.activities.SearchComicsActivity;
import com.testapp.marvelcomics.presentation.ui.adapters.HistoryListAdapter;
import com.testapp.marvelcomics.presentation.views.HistorySearchListView;

import java.util.List;

import javax.inject.Inject;

public class SearchHistoryFragment extends Fragment implements HistorySearchListView {

    @Inject
    HistoryListPresenter presenter;
    private View rootView;
    private RecyclerView recyclerView;
    private HistoryListAdapter comicsAdapter;
    private View emptyListView;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        MarvelComicsApplication.getAppComponent().inject(this);

        presenter.bindLifecycleOwner(this);
        presenter.bindView(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_history_list, container, false);

        recyclerView = rootView.findViewById(R.id.historyList);
        comicsAdapter = new HistoryListAdapter(getContext());
        comicsAdapter.setListener(searchRequestParams -> {
            presenter.historySearchItemSelected(searchRequestParams);
            try {
                ((SearchComicsActivity) getActivity()).setCurrentTab(0);
            } catch (Exception exception) {
                exception.printStackTrace();
            }
        });

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(comicsAdapter);

        emptyListView = rootView.findViewById(R.id.empty_list_hint);

        presenter.loadSearchHistory();

        return rootView;
    }

    @Override
    public void showHistoryList(List<SearchRequestParams> historyList) {
        recyclerView.setVisibility(historyList.isEmpty() ? View.GONE : View.VISIBLE);
        emptyListView.setVisibility(historyList.isEmpty() ? View.VISIBLE : View.GONE);
        if (!historyList.isEmpty()) {
            comicsAdapter.addItems(historyList);
        }
    }

    @Override
    public void clearHistoryList() {
        comicsAdapter.clearAdapter();
    }

    @Override
    public void showError() {
        Snackbar.make(rootView, getString(R.string.error_message), Snackbar.LENGTH_LONG).show();
    }
}
