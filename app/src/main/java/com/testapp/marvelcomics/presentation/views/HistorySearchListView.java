package com.testapp.marvelcomics.presentation.views;

import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.List;

public interface HistorySearchListView {

    void showHistoryList(List<SearchRequestParams> searchRequestParamsList);

    void clearHistoryList();

    void showError();
}
