package com.testapp.marvelcomics.presentation.models;

import com.testapp.marvelcomics.common.MarvelComicsApplication;
import com.testapp.marvelcomics.domain.SearchInteractor;
import com.testapp.marvelcomics.domain.SearchModel;
import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public class ComicsSearchModel implements SearchModel {

    @Inject
    SearchInteractor searchInteractor;

    private SearchListPresenter searchListPresenter;

    public ComicsSearchModel() {
        MarvelComicsApplication.getAppComponent().inject(this);
    }

    public ComicsSearchModel(SearchInteractor searchInteractor, SearchListPresenter searchListPresenter) {
        this.searchInteractor = searchInteractor;
        this.searchListPresenter = searchListPresenter;
    }

    @Override
    public Observable<List<Comic>> requestComicsBySearchParams(SearchRequestParams searchRequestParams, boolean isSaveSearchRequest) {
        if (isSaveSearchRequest) {
            searchInteractor.saveSearchRequest(searchRequestParams);
        }
        return searchInteractor.requestComicsBySearchParams(searchRequestParams);
    }

    @Override
    public Observable<List<Comic>> requestMoreComicsByPreviousSearchRequest() {
        return searchInteractor.requestMoreComicsByPreviousSearchRequest();
    }

    @Override
    public Flowable<List<SearchRequestParams>> getSearchHistory() {
        return searchInteractor.getSearchHistory();
    }

    @Override
    public void historyItemSelected(SearchRequestParams historyItem) {
        if (searchListPresenter != null) {
            searchListPresenter.requestComicsBySearchParams(historyItem, false);
        }
    }

    @Override
    public void bindSearchListPresenter(SearchListPresenter searchListPresenter) {
        this.searchListPresenter = searchListPresenter;
    }
}
