package com.testapp.marvelcomics.presentation.ui.activities;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.testapp.marvelcomics.R;
import com.testapp.marvelcomics.domain.dto.AnnotationComicFormat;
import com.testapp.marvelcomics.domain.dto.AnnotationOrderBy;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.ui.fragments.SearchResultsListFragment;

import java.io.Serializable;

public class SearchFiltersActivity extends AppCompatActivity {

    private SearchRequestParams searchRequestParams;
    private EditText editTextTitleView;
    private EditText editTextTitleStartWithView;
    private EditText editTextIssueNumber;
    private Spinner spinnerFormatView;
    private Spinner spinnerOrderByView;
    private CheckBox checkBoxNoVariants;
    private String[] comicFormatArray;
    private String[] orderByArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filters);
        Serializable requestParams = getIntent().getExtras().getSerializable(SearchResultsListFragment.FILTER_PARAMS_PARAM_KEY);
        if (requestParams != null) {
            searchRequestParams = (SearchRequestParams) requestParams;
        }

        Toolbar toolbar = findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }

        Drawable toolbarIcon = toolbar.getNavigationIcon();
        if (toolbarIcon != null) {
            toolbar.getNavigationIcon().setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        }

        comicFormatArray = getResources().getStringArray(R.array.comic_format);
        orderByArray = getResources().getStringArray(R.array.comic_order_by);

        editTextTitleView = findViewById(R.id.editTextTitle);
        editTextTitleStartWithView = findViewById(R.id.editTextTitleStartWith);
        editTextIssueNumber = findViewById(R.id.editTextIssueNumber);

        spinnerFormatView = findViewById(R.id.spinnerFormat);
        final ArrayAdapter<CharSequence> formatAdapter = ArrayAdapter.createFromResource(this, R.array.comic_format, android.R.layout.simple_spinner_item);
        formatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerFormatView.setAdapter(formatAdapter);
        spinnerFormatView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                CharSequence selectedItemName = formatAdapter.getItem(i);
                if (selectedItemName != null) {
                    if (selectedItemName.toString().equals(comicFormatArray[0])) {
                        searchRequestParams.format = null;
                    } else {
                        searchRequestParams.format = new AnnotationComicFormat(selectedItemName.toString());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinnerOrderByView = findViewById(R.id.spinnerOrderBy);
        final ArrayAdapter<CharSequence> orderByAdapter = ArrayAdapter.createFromResource(this, R.array.comic_order_by, android.R.layout.simple_spinner_item);
        orderByAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerOrderByView.setAdapter(orderByAdapter);
        spinnerOrderByView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                CharSequence selectedItemName = orderByAdapter.getItem(i);
                if (selectedItemName != null) {
                    if (selectedItemName.toString().equals(orderByArray[0])) {
                        searchRequestParams.orderBy = null;
                    } else {

                        searchRequestParams.orderBy = new AnnotationOrderBy(selectedItemName.toString());
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        checkBoxNoVariants = findViewById(R.id.checkBoxNoVariants);

        findViewById(R.id.buttonApply).setOnClickListener(view -> {
            String title = editTextTitleView.getText().toString().trim();
            if (!title.isEmpty()) {
                searchRequestParams.title = title;
            } else {
                searchRequestParams.title = null;
            }

            String titleStartWith = editTextTitleStartWithView.getText().toString().trim();
            if (!titleStartWith.isEmpty()) {
                searchRequestParams.titleStartsWith = titleStartWith;
            } else {
                searchRequestParams.titleStartsWith = null;
            }

            String issueNumber = editTextIssueNumber.getText().toString().trim();
            if (!issueNumber.isEmpty()) {
                searchRequestParams.issueNumber = Integer.parseInt(issueNumber);
            } else {
                searchRequestParams.issueNumber = -1;
            }

            searchRequestParams.noVariants = checkBoxNoVariants.isChecked();

            Intent intent = new Intent();
            intent.putExtra(SearchResultsListFragment.FILTER_PARAMS_PARAM_KEY, searchRequestParams);
            setResult(RESULT_OK, intent);
            finish();
        });

        updateFiltersViewsData();
    }

    private void updateFiltersViewsData() {
        if (searchRequestParams.title != null) {
            editTextTitleView.setText(searchRequestParams.title);
        }

        if (searchRequestParams.titleStartsWith != null) {
            editTextTitleStartWithView.setText(searchRequestParams.titleStartsWith);
        }

        if (searchRequestParams.issueNumber != -1) {
            editTextIssueNumber.setText(String.valueOf(searchRequestParams.issueNumber));
        }

        if (searchRequestParams.format != null) {
            for (int i = 0; i < comicFormatArray.length; i++) {
                if (comicFormatArray[i].equalsIgnoreCase(searchRequestParams.format.getFormat())) {
                    spinnerFormatView.setSelection(i);
                }
            }
        }

        if (searchRequestParams.orderBy != null) {
            for (int i = 0; i < orderByArray.length; i++) {
                if (orderByArray[i].replace(" ","").equalsIgnoreCase(searchRequestParams.orderBy.getOrderBy())) {
                    spinnerOrderByView.setSelection(i);
                }
            }
        }

        checkBoxNoVariants.setChecked(searchRequestParams.noVariants);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
