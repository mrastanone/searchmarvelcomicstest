package com.testapp.marvelcomics.domain.dto;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.testapp.marvelcomics.data.remote.MarvelRemoteStorage;

import java.io.Serializable;
import java.util.HashMap;

@Entity
public class SearchRequestParams implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int id;

    public String title;

    public String titleStartsWith;

    public AnnotationComicFormat format;

    public AnnotationOrderBy orderBy;

    public int issueNumber = -1;

    public boolean noVariants = false;

    public long saveTimeMilliseconds = 0;

    public HashMap<String, String> convertParamsToMap() {
        HashMap<String, String> result = new HashMap<>(10);
        if (title != null) {
            result.put(MarvelRemoteStorage.TITLE, title);
        }
        if (titleStartsWith != null) {
            result.put(MarvelRemoteStorage.TITLE_START_WITH, titleStartsWith);
        }
        if (format != null) {
            result.put(MarvelRemoteStorage.FORMAT, format.getFormat());
        }
        if (orderBy != null) {
            result.put(MarvelRemoteStorage.ORDER_BY, orderBy.getOrderBy());
        }
        if (issueNumber != -1) {
            result.put(MarvelRemoteStorage.ISSUE_NUMBER, String.valueOf(issueNumber));
        }
        if (noVariants) {
            result.put(MarvelRemoteStorage.NO_VARIANTS, String.valueOf(noVariants));
        }
        return result;
    }

}
