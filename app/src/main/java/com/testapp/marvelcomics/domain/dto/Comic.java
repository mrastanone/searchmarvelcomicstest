package com.testapp.marvelcomics.domain.dto;

import com.google.gson.annotations.SerializedName;

public class Comic {

    @SerializedName("id")
    public int id;

    @SerializedName("title")
    public String title;

    @SerializedName("description")
    public String description;

    @SerializedName("thumbnail")
    public Thumbnail thumbnail;

    public class Thumbnail {

        @SerializedName("path")
        public String path;

        @SerializedName("extension")
        public String extension;
    }
}
