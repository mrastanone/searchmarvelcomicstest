package com.testapp.marvelcomics.domain;

import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public interface SearchRepository {

    Observable<List<Comic>> requestComicsBySearchParams(SearchRequestParams searchKey);

    Observable<List<Comic>> requestMoreComicsByPreviousSearch();

    Flowable<List<SearchRequestParams>> getSearchHistory();

    void saveSearchRequest(SearchRequestParams searchRequestParams);

}
