package com.testapp.marvelcomics.domain.dto;

import android.support.annotation.StringDef;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class AnnotationOrderBy implements Serializable {

    public static final String ON_SALE_DATA = "onsaleDate";
    public static final String TITLE = "title";
    public static final String ISSUE_NUMBER = "issueNumber";
    public static final String FOC_DATE = "focDate";

    private @ComicOrderBy
    String orderBy;

    public AnnotationOrderBy(@ComicOrderBy String orderBy) {
        setOrderBy(orderBy);
    }

    public @ComicOrderBy
    String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(@ComicOrderBy String orderBy) {
        if (orderBy.replace(" ", "").equalsIgnoreCase(ON_SALE_DATA)) {
            this.orderBy = ON_SALE_DATA;
        } else if (orderBy.replace(" ", "").equalsIgnoreCase(TITLE)) {
            this.orderBy = TITLE;
        } else if (orderBy.replace(" ", "").equalsIgnoreCase(ISSUE_NUMBER)) {
            this.orderBy = ISSUE_NUMBER;
        } else if (orderBy.replace(" ", "").equalsIgnoreCase(FOC_DATE)) {
            this.orderBy = FOC_DATE;
        }
    }

    @StringDef({ON_SALE_DATA, TITLE, ISSUE_NUMBER, FOC_DATE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ComicOrderBy {

    }
}
