package com.testapp.marvelcomics.domain;

import com.testapp.marvelcomics.common.MarvelComicsApplication;
import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public class ComicsSearchInteractor implements SearchInteractor {

    @Inject
    SearchRepository searchRepository;

    public ComicsSearchInteractor() {
        MarvelComicsApplication.getAppComponent().inject(this);
    }


    @Override
    public Observable<List<Comic>> requestComicsBySearchParams(SearchRequestParams searchRequestParams) {
        return searchRepository.requestComicsBySearchParams(searchRequestParams);
    }

    @Override
    public Observable<List<Comic>> requestMoreComicsByPreviousSearchRequest() {
        return searchRepository.requestMoreComicsByPreviousSearch();
    }

    @Override
    public Flowable<List<SearchRequestParams>> getSearchHistory() {
        return searchRepository.getSearchHistory();
    }

    @Override
    public void saveSearchRequest(SearchRequestParams searchRequestParams) {
        searchRepository.saveSearchRequest(searchRequestParams);
    }
}
