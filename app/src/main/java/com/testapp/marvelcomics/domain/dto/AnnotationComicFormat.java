package com.testapp.marvelcomics.domain.dto;

import android.support.annotation.StringDef;

import java.io.Serializable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class AnnotationComicFormat implements Serializable {

    public static final String COMIC = "comic";
    public static final String MAGAZINE = "magazine";
    public static final String TRADE_PAPERBACK = "trade paperback";
    public static final String HARDCOVER = "hardcover";
    public static final String DIGEST = "digest";
    public static final String GRAPHIC_NOVEL = "graphic novel";
    public static final String DIGITAL_COMIC = "digital comic";
    public static final String INFINITE_COMIC = "infinite comic";

    private @ComicFormat
    String format;

    @StringDef({COMIC, MAGAZINE, TRADE_PAPERBACK, HARDCOVER, DIGEST, GRAPHIC_NOVEL, DIGITAL_COMIC, INFINITE_COMIC})
    @Retention(RetentionPolicy.SOURCE)
    public @interface ComicFormat {

    }

    public AnnotationComicFormat(@ComicFormat String format) {
        if (format.equalsIgnoreCase(COMIC)) {
            this.format = COMIC;
        } else if (format.equalsIgnoreCase(MAGAZINE)) {
            this.format = MAGAZINE;
        } else if (format.equalsIgnoreCase(TRADE_PAPERBACK)) {
            this.format = TRADE_PAPERBACK;
        } else if (format.equalsIgnoreCase(HARDCOVER)) {
            this.format = HARDCOVER;
        } else if (format.equalsIgnoreCase(DIGEST)) {
            this.format = DIGEST;
        } else if (format.equalsIgnoreCase(GRAPHIC_NOVEL)) {
            this.format = GRAPHIC_NOVEL;
        } else if (format.equalsIgnoreCase(DIGITAL_COMIC)) {
            this.format = DIGITAL_COMIC;
        } else if (format.equalsIgnoreCase(INFINITE_COMIC)) {
            this.format = INFINITE_COMIC;
        }
    }

    public @ComicFormat
    String getFormat() {
        return format;
    }

    public void setFormat(@ComicFormat String format) {
        this.format = format;
    }
}
