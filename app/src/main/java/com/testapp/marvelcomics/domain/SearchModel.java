package com.testapp.marvelcomics.domain;

import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.models.SearchListPresenter;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public interface SearchModel {

    Observable<List<Comic>> requestComicsBySearchParams(SearchRequestParams searchRequestParams, boolean isSaveRequest);

    Observable<List<Comic>> requestMoreComicsByPreviousSearchRequest();

    Flowable<List<SearchRequestParams>> getSearchHistory();

    void historyItemSelected(SearchRequestParams historyItem);

    void bindSearchListPresenter(SearchListPresenter searchListPresenter);
}
