package com.testapp.marvelcomics.domain;

import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public interface SearchInteractor {

    Observable<List<Comic>> requestComicsBySearchParams(SearchRequestParams searchRequestParams);

    Observable<List<Comic>> requestMoreComicsByPreviousSearchRequest();

    Flowable<List<SearchRequestParams>> getSearchHistory();

    void saveSearchRequest(SearchRequestParams searchRequestParams);
}
