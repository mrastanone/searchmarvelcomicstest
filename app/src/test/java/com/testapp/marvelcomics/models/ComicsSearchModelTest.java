package com.testapp.marvelcomics.models;

import com.testapp.marvelcomics.domain.SearchInteractor;
import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.models.ComicsSearchModel;
import com.testapp.marvelcomics.presentation.models.SearchListPresenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ComicsSearchModelTest {


    private SearchInteractor mockedSearchInteractor;
    private SearchListPresenter mockedSearchListPresenter;
    private ComicsSearchModel model;


    @Before
    public void init() {
        mockedSearchInteractor = mock(SearchInteractor.class);
        mockedSearchListPresenter = mock(SearchListPresenter.class);
        model = new ComicsSearchModel(mockedSearchInteractor, mockedSearchListPresenter);
    }

    @Test
    public void test_requestComicsBySearchParamsWithSaving() {
        SearchRequestParams testRequestParams = new SearchRequestParams();
        Observable<List<Comic>> observable = Observable.just(new ArrayList<>());

        when(mockedSearchInteractor.requestComicsBySearchParams(testRequestParams)).thenReturn(observable);

        Observable<List<Comic>> observableResult = model.requestComicsBySearchParams(testRequestParams, true);

        verify(mockedSearchInteractor).saveSearchRequest(testRequestParams);
        assertEquals(observable, observableResult);
    }

    @Test
    public void test_requestComicsBySearchParamsWithoutSaving() {
        SearchRequestParams testRequestParams = new SearchRequestParams();
        Observable<List<Comic>> observable = Observable.just(new ArrayList<>());

        when(mockedSearchInteractor.requestComicsBySearchParams(testRequestParams)).thenReturn(observable);

        Observable<List<Comic>> observableResult = model.requestComicsBySearchParams(testRequestParams, false);

        verify(mockedSearchInteractor, never()).saveSearchRequest(testRequestParams);
        assertEquals(observable, observableResult);
    }

}
