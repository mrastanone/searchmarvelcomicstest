package com.testapp.marvelcomics.presenters;

import com.testapp.marvelcomics.data.remote.NoInternetException;
import com.testapp.marvelcomics.domain.SearchModel;
import com.testapp.marvelcomics.domain.dto.Comic;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.presenters.ComicsSearchListPresenter;
import com.testapp.marvelcomics.presentation.views.SearchResultsListView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ComicsSearchListPresenterTest {

    private SearchResultsListView mockedSearchResultsListView;
    private SearchModel mockedSearchModel;
    private ComicsSearchListPresenter presenter;

    @Before
    public void init() {
        mockedSearchResultsListView = mock(SearchResultsListView.class);
        mockedSearchModel = mock(SearchModel.class);
        presenter = new ComicsSearchListPresenter(mockedSearchModel);
        presenter.bindView(mockedSearchResultsListView);
    }

    @Test
    public void test_requestComicsBySearchParamsHasData() {
        SearchRequestParams testRequestParams = new SearchRequestParams();
        ArrayList<Comic> testDataList = new ArrayList<>();
        Observable<List<Comic>> observable = Observable.just(testDataList);

        when(mockedSearchModel.requestComicsBySearchParams(testRequestParams, false)).thenReturn(observable);

        presenter.requestComicsBySearchParams(testRequestParams, false);

        verify(mockedSearchResultsListView).clearComicsList();
        verify(mockedSearchResultsListView).setActualSearchParameters(testRequestParams);
        verify(mockedSearchResultsListView).addComics(testDataList);
    }

    @Test
    public void test_requestComicsBySearchParamsError() {
        SearchRequestParams testRequestParams = new SearchRequestParams();
        Observable<List<Comic>> observable = Observable.error(new Throwable());
        when(mockedSearchModel.requestComicsBySearchParams(testRequestParams, false)).thenReturn(observable);

        presenter.requestComicsBySearchParams(testRequestParams, false);

        verify(mockedSearchResultsListView).showError();
    }

    @Test
    public void test_requestComicsBySearchParamsNoInternet() {
        SearchRequestParams testRequestParams = new SearchRequestParams();
        Observable<List<Comic>> observable = Observable.error(new NoInternetException());
        when(mockedSearchModel.requestComicsBySearchParams(testRequestParams, false)).thenReturn(observable);

        presenter.requestComicsBySearchParams(testRequestParams, false);

        verify(mockedSearchResultsListView).showNoInternetConnectionMessage();
    }

    @Test
    public void test_loadMoreComicsHasData() {
        ArrayList<Comic> testDataList = new ArrayList<>();
        Observable<List<Comic>> observable = Observable.just(testDataList);

        when(mockedSearchModel.requestMoreComicsByPreviousSearchRequest()).thenReturn(observable);

        presenter.loadMoreComics();

        verify(mockedSearchResultsListView).addComics(testDataList);
    }

    @Test
    public void test_loadMoreComicsError() {
        Observable<List<Comic>> observable = Observable.error(new Throwable());
        when(mockedSearchModel.requestMoreComicsByPreviousSearchRequest()).thenReturn(observable);

        presenter.loadMoreComics();

        verify(mockedSearchResultsListView).showError();
    }

    @Test
    public void test_loadMoreComicsNoInternet() {
        Observable<List<Comic>> observable = Observable.error(new NoInternetException());
        when(mockedSearchModel.requestMoreComicsByPreviousSearchRequest()).thenReturn(observable);

        presenter.loadMoreComics();

        verify(mockedSearchResultsListView).showNoInternetConnectionMessage();
    }
}
