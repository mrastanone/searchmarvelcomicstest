package com.testapp.marvelcomics.presenters;

import com.testapp.marvelcomics.domain.SearchModel;
import com.testapp.marvelcomics.domain.dto.SearchRequestParams;
import com.testapp.marvelcomics.presentation.presenters.ComicsHistoryListPresenter;
import com.testapp.marvelcomics.presentation.views.HistorySearchListView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ComicsHistoryListPresenterTest {


    private HistorySearchListView mockedHistorySearchListView;
    private SearchModel mockedSearchModel;
    private ComicsHistoryListPresenter presenter;


    @Before
    public void init() {
        mockedHistorySearchListView = mock(HistorySearchListView.class);
        mockedSearchModel = mock(SearchModel.class);
        presenter = new ComicsHistoryListPresenter(mockedSearchModel);
        presenter.bindView(mockedHistorySearchListView);
    }

    @Test
    public void test_loadSearchHistoryHasData() {
        List<SearchRequestParams> emptyList = new ArrayList<>();
        Flowable<List<SearchRequestParams>> flowable = Flowable.just(emptyList);

        when(mockedSearchModel.getSearchHistory()).thenReturn(flowable);
        presenter.loadSearchHistory();

        verify(mockedSearchModel).getSearchHistory();
        verify(mockedHistorySearchListView).clearHistoryList();
        verify(mockedHistorySearchListView).showHistoryList(emptyList);
    }

    @Test
    public void test_loadSearchHistoryError() {
        Flowable<List<SearchRequestParams>> flowable = Flowable.error(new Throwable());

        when(mockedSearchModel.getSearchHistory()).thenReturn(flowable);
        presenter.loadSearchHistory();

        verify(mockedSearchModel).getSearchHistory();
        verify(mockedHistorySearchListView).showError();
    }

    @Test
    public void test_historySearchItemSelected() {
        SearchRequestParams testParams = new SearchRequestParams();
        presenter.historySearchItemSelected(testParams);
        verify(mockedSearchModel).historyItemSelected(testParams);
    }
}
